﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Account;

namespace LedgerApp
{

    class Program
    {    
        static void Main(string[] args)
        {            
            var attempts = 0;
            var auth = false;
            var selection = 0;
            double deposit = 0;
            double withdrawl = 0;
            Account.Account account = new Account.Account();
            var balanceSheet = account.CreateTable();
            var u = "";
            var pw = "";

            User user = new User();
            user.CreateTable();

            do
            {
                selection = LoginMenu;
                if (selection == 1)
                {
                    Console.WriteLine("Enter username:");
                    u = Console.ReadLine();
                    Console.WriteLine("Enter a password:");
                    pw = Console.ReadLine();
                    user.AddUser(u, pw);
                }

                if (selection == 2)
                {
                    break;
                }
            }
            while (selection < 2);

            Console.Clear();

            for (int i = 0; i < 3; i++)
            {
                Console.WriteLine("Username");
                u = Console.ReadLine();
                Console.WriteLine("Password");
                pw = Console.ReadLine();

                if (!user.Authorized(u, pw))
                {
                    Console.Clear();
                    Console.WriteLine("Login incorrect");
                    attempts++;
                }
                else
                {
                    break;
                }
            }

            auth = (attempts < 3);
            
            Console.Clear();

            if (!auth)
            {
                Console.WriteLine("Login unsuccessful");
                Console.ReadKey();            
            }

            do
            {
                selection = TransactionMenu;
                switch (selection)
                {
                    case 1:
                        Console.WriteLine("Enter Deposit Amount:");
                        deposit = Convert.ToDouble(Console.ReadLine());
                        balanceSheet.Rows.Add(u, DateTime.Now, deposit);
                        break;
                    case 2:
                        Console.WriteLine("Enter Withdrawl Amount:");
                        withdrawl = Convert.ToDouble(Console.ReadLine());
                        balanceSheet.Rows.Add(u, DateTime.Now, -withdrawl);
                        break;
                    case 3:                        
                        Console.WriteLine("Current Balance is: $" + account.GetBalance(u));
                        Console.ReadKey();
                        break;
                    case 4:
                        foreach (DataRow row in balanceSheet.Rows)
                        {
                            Console.WriteLine();
                            for(int i = 0; i < balanceSheet.Columns.Count; i++)
                            {                                
                                Console.Write(row[i].ToString() + " ");
                            }                            
                        }
                        Console.ReadKey();
                        break;
                    default:
                        break;
                };
            }
            while (selection != 5);

            Console.ReadKey();
        }

        public static int LoginMenu
        {
            get
            {
                Console.Clear();
                Console.WriteLine("Welcome");
                Console.WriteLine();
                Console.WriteLine("1. Create New Account");
                Console.WriteLine();
                Console.WriteLine("2. Login to Account");

                var result = Console.ReadLine();
                try
                {
                    return Convert.ToInt32(result);
                }
                catch
                {
                    Console.WriteLine("Incorrect selection");
                    return 0;
                }
            }
        }

        public static int TransactionMenu
        {
            get
            {
                Console.Clear();
                Console.WriteLine("Select One:");
                Console.WriteLine();
                Console.WriteLine("1. Deposit Funds");
                Console.WriteLine("2. Withdraw Funds");
                Console.WriteLine("3. Check Balance");
                Console.WriteLine("4. View Transaction History");
                Console.WriteLine("5. Logout");

                var result = Console.ReadLine();
                try
                {
                    return Convert.ToInt32(result);
                }
                catch
                {
                    Console.WriteLine("Incorrect selection");
                    return 0;
                }
            }
        }
    }
}
