﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Account
{
    public class Account
    {
        public DataTable BalanceSheet { get; set; } = new DataTable("BalanceSheet");

        public DataTable CreateTable()
        {
            BalanceSheet.Columns.Add("user", typeof(string));
            BalanceSheet.Columns.Add("date", typeof(DateTime));
            BalanceSheet.Columns.Add("amount", typeof(double));
            return BalanceSheet;
        }

        public double GetBalance(string user) => Convert.ToDouble(BalanceSheet.Compute("Sum(amount)", "user = '" + user + "'"));
    }

    public class User
    {
        public DataTable Users { get; set; } = new DataTable("Users");
        public string[] Credentials { get; set; }

        public void CreateTable()
        {
            Users.Columns.Add("userid", typeof(string));
            Users.Columns.Add("password", typeof(string));
        }

        public void AddUser(string userid, string password)
        {
            if (userid != "" && password != "")
            {
                Users.Rows.Add(userid, password);
            }
        }

        public bool Authorized(string uid, string pw)
        {
            var result = Users.AsEnumerable().FirstOrDefault(t => t.Field<string>("userid") == uid && t.Field<string>("password") == pw);
            return result != null;

        }
    }
}
